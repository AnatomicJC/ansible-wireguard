#!/bin/sh

set -eu

_wg () {
  docker run -i --rm --entrypoint /usr/bin/wg linuxserver/wireguard "${1}"
}

_ansible_vault () {
  ANSIBLE_VAULT_PASSWORD_FILE="${1}" ansible-vault encrypt | grep -v successful
}

PREFIX="${1:-}"
SPACES="${2:-0}"
SPACES_VAULT="$((${SPACES}+2))"
VAULT_FILE="${3:-}"

PRIVATE=$(_wg genkey)
PUBLIC=$(printf "${PRIVATE}" | _wg pubkey)

if [ -n "${VAULT_FILE}" ]
then
  PRIVATE_VAULTED=$(printf ${PRIVATE} | _ansible_vault ${VAULT_FILE})
  PUBLIC_VAULTED=$(printf ${PUBLIC} | _ansible_vault ${VAULT_FILE})
  echo "$(printf "%${SPACES}s" "")${PREFIX:-}public_key: !vault |"
  for LINE in ${PUBLIC_VAULTED}
  do
    echo "$(printf "%${SPACES_VAULT}s" "")${LINE}"
  done
  echo "$(printf "%${SPACES}s" "")${PREFIX:-}private_key: !vault |"
  for LINE in ${PRIVATE_VAULTED}
  do
    echo "$(printf "%${SPACES_VAULT}s" "")${LINE}"
  done
else
  echo "$(printf "%${SPACES}s" "")${PREFIX:-}public_key: ${PUBLIC}"
  printf "$(printf "%${SPACES}s" "")${PREFIX:-}private_key: ${PRIVATE}"
fi
